package powa.prucco.voosasuggests.tabelle;

import java.io.Serializable;

/**
 * Classe per la rappresentazione delle esperienze dell'utente
 *
 * Created by Andrea on 08/08/2015.
 */
public class Valutazione implements Serializable{

    private long id;
    private String datada;
    private String dataa;
    private long id_struttura;
    private double rate;
    private String descrizione;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDatada() {
        return datada;
    }

    public void setDatada(String datada) {
        this.datada = datada;
    }

    public String getDataa() {
        return dataa;
    }

    public void setDataa(String dataa) {
        this.dataa = dataa;
    }

    public long getIdStruttura() {
        return id_struttura;
    }

    public void setIdStruttura(long idStruttura) {
        this.id_struttura = idStruttura;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
}
