package powa.prucco.voosasuggests.tabelle;

import java.io.Serializable;

/**
 * Classe per la rappresentazione delle categorie
 *
 * Created by Andrea on 08/08/2015.
 */
public class Categoria implements Serializable {

    private long id;
    private String nome;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
