package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBColumns;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Valutazione;
import powa.prucco.voosasuggests.utility.AppUtils;
import powa.prucco.voosasuggests.utility.ValutazioneAdapter;

public class ListaValutazioni extends AppCompatActivity {

    private ListView listValutazioni;

    private static ValutazioneAdapter valutazioneAdapter = null;
    private static List<Valutazione> valutazioneList = new ArrayList<>();
    private DBManager dbManager;
    private long id_struttura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_valutazioni);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DBManager(this);

        valutazioneAdapter = new ValutazioneAdapter(this, valutazioneList);

        listValutazioni = (ListView) findViewById(R.id.listValutazioni);
        listValutazioni.setAdapter(valutazioneAdapter);

        Intent intent = getIntent();

        if(intent != null && intent.hasExtra(AppUtils.STRUTTURA)) {
            id_struttura = intent.getLongExtra(AppUtils.STRUTTURA, -1);
        }
        else {
            Toast.makeText(ListaValutazioni.this, "No id struttura", Toast.LENGTH_LONG).show();
            finish();
        }

        if(valutazioneList.isEmpty())
            caricaValutazioni();

        impostaListenerListValutazioni();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_valutazioni, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.aggiungivalutazione) {
            Intent intent = new Intent(ListaValutazioni.this, AggiungiOModificaValutazione.class);
            intent.putExtra(AggiungiOModificaValutazione.AGGIUNGI, id_struttura);
            startActivityForResult(intent, AppUtils.RICHIESTA_AGGIUNTA_VALUTAZIONE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == AppUtils.RICHIESTA_AGGIUNTA_VALUTAZIONE) {

            Valutazione valutazione = (Valutazione) data.getSerializableExtra(AppUtils.VALUTAZIONE);
            id_struttura = valutazione.getIdStruttura();
            valutazioneList.add(valutazione);
            valutazioneAdapter.notifyDataSetChanged();
        } else if (resultCode == RESULT_OK && requestCode == AppUtils.RICHIESTA_MODIFICA_VALUTAZIONE) {

            Valutazione valutazione = (Valutazione) data.getSerializableExtra(AppUtils.VALUTAZIONE);
            id_struttura = valutazione.getId();
            valutazioneList.clear();
            caricaValutazioni();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!valutazioneList.isEmpty()) {
            valutazioneList.clear();
            caricaValutazioni();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        if(v.getId() == R.id.listValutazioni){
            String header = valutazioneList.get(info.position).getDatada();
            if(valutazioneList.get(info.position).getDataa() != null)
                header = header + " - " + valutazioneList.get(info.position).getDataa();
            menu.setHeaderTitle(header);
            String[] azioni = getResources().getStringArray(R.array.options);
            for(int i = 0; i < azioni.length; i++)
                menu.add(Menu.NONE, i, i, azioni[i]);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] azioni = getResources().getStringArray(R.array.options);
        String azione = azioni[menuItemIndex];

        final Valutazione valutazione = valutazioneList.get(info.position);

        switch(azione){
            case "Modifica" : {
                Intent perModifica = new Intent(ListaValutazioni.this, AggiungiOModificaStruttura.class);
                perModifica.putExtra(AggiungiOModificaValutazione.MODIFICA, valutazione);
                startActivityForResult(perModifica, AppUtils.RICHIESTA_MODIFICA_VALUTAZIONE);
                break;
            }
            case "Elimina" : {

                AlertDialog.Builder builder = new AlertDialog.Builder(ListaValutazioni.this);
                builder.setTitle("Cancellazione");
                builder.setMessage("Sei sicuro di voler eliminare la valutazione?");
                builder.setPositiveButton(R.string.sicuro, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int risultato = dbManager.eliminaTupla(DBColumns.V_TABLE, valutazione.getIdStruttura(),  valutazione.getDatada());

                        if (risultato == 1) {
                            //la cancellazione è avvenuta con successo
                            Toast.makeText(getApplicationContext(), "Eliminazione avvenuta con successo.", Toast.LENGTH_SHORT).show();
                            valutazioneList.remove(valutazione);
                            valutazioneAdapter.notifyDataSetChanged();
                        } else
                            Toast.makeText(getApplicationContext(), "Non è stato possibile eliminare la valutazione", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton(R.string.no_sicuro, null);

                builder.show();
            }
        }

        return true;
    }

    //funzione per l'assegnazione dei listenere alla listView
    private void impostaListenerListValutazioni() {

        listValutazioni.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Valutazione valutazione = (Valutazione) adapterView.getItemAtPosition(i);

                Intent intent = new Intent(ListaValutazioni.this, DettaglioValutazione.class);
                intent.putExtra(AppUtils.VALUTAZIONE, valutazione);
                startActivity(intent);
            }
        });

        registerForContextMenu(listValutazioni);
    }

    private void caricaValutazioni(){

        Cursor recupero = dbManager.recuperaTuple(DBColumns.V_TABLE, DBColumns.V_STR + "= ?",
                new String[]{Long.toString(id_struttura)});

        if(recupero == null) {
            Toast.makeText(this, "Impossibile caricare dati dal database.", Toast.LENGTH_SHORT).show();
            return;
        } else if(!recupero.moveToFirst()){
            Toast.makeText(this, "Nessuna valutazione presente nel database per la struttura.", Toast.LENGTH_SHORT).show();
            return;
        }

        for(int i = 0; i < recupero.getCount(); i++){
            long id = recupero.getLong(recupero.getColumnIndex(DBColumns.V_ID));
            String datada = recupero.getString(recupero.getColumnIndex(DBColumns.V_DATADA));
            String dataa = recupero.getString(recupero.getColumnIndex(DBColumns.V_DATAA));
            long idStruttura = recupero.getLong(recupero.getColumnIndex(DBColumns.V_STR));
            double rate = recupero.getDouble(recupero.getColumnIndex(DBColumns.V_RATE));
            String descrizione = recupero.getString(recupero.getColumnIndex(DBColumns.V_DESC));

            Valutazione valutazione = new Valutazione();

            valutazione.setId(id);
            valutazione.setDatada(datada);
            valutazione.setDataa(dataa);
            valutazione.setIdStruttura(idStruttura);
            valutazione.setRate(rate);
            valutazione.setDescrizione(descrizione);

            valutazioneList.add(valutazione);

            recupero.moveToNext();
        }

        valutazioneAdapter.notifyDataSetChanged();

    }
}
