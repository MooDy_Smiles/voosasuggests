package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBColumns;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Struttura;
import powa.prucco.voosasuggests.utility.AppUtils;

public class DettaglioStruttura extends AppCompatActivity {

    private DBManager dbManager;
    private Intent intent;

    private ImageView imageStruttura;
    private TextView labNome, labCategoria, labIndirizzo, labRating, labTel, labCell, labMail, labWeb, descrizione, indirizzo, tel, cell, mail, web;
    private Button esperienze;
    private Struttura struttura;

    private View.OnClickListener telOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent chiamata = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel.getText().toString().trim(), null));
            startActivity(chiamata);
        }
    };

    private View.OnClickListener cellOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DettaglioStruttura.this);
            builder.setTitle("Scegli come completare l'azione");

            String[] items = new String[]{"Chiamata", "Messaggio"};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 1: {

                            Intent messaggio = new Intent(Intent.ACTION_VIEW);
                            messaggio.setType("vnd.android-dir/mms-sms");
                            messaggio.putExtra("address", cell.getText().toString().trim());
                            startActivity(messaggio);
                            break;
                        }
                        case 0: {

                            Intent chiamata = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", cell.getText().toString().trim(), null));
                            startActivity(chiamata);
                            break;
                        }
                    }
                }
            });

            builder.show();
        }
    };

    private View.OnClickListener mailOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(AppUtils.connessioneInternet(DettaglioStruttura.this)) {

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, mail.getText().toString().trim());

                startActivity(Intent.createChooser(intent, "Invia email"));
            }else
                Toast.makeText(DettaglioStruttura.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener webOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(AppUtils.connessioneInternet(DettaglioStruttura.this)) {

                Intent sito = new Intent(Intent.ACTION_VIEW);
                sito.setData(Uri.parse("http://" + web.getText().toString().trim()));
                startActivity(Intent.createChooser(sito, "Scegli azione"));
            }else
                Toast.makeText(DettaglioStruttura.this, R.string.no_internet, Toast.LENGTH_SHORT).show();

        }
    };

    private View.OnClickListener indirizzoOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(AppUtils.connessioneInternet(DettaglioStruttura.this)){

                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+indirizzo.getText().toString());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }else
                Toast.makeText(DettaglioStruttura.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dettaglio_struttura);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DBManager(this);

        inizializzaElementi();

        //recupero l'intent passato da ListaStrutture
        intent = getIntent();

        if(intent != null && intent.hasExtra(AppUtils.STRUTTURA))
            popolaElementi(intent, AppUtils.STRUTTURA);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dettaglio_struttura, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.modificastruttura){

            Intent perModifica = new Intent(DettaglioStruttura.this, AggiungiOModificaStruttura.class);
            perModifica.putExtra(AggiungiOModificaStruttura.MODIFICA, struttura);
            startActivityForResult(perModifica, AppUtils.RICHIESTA_MODIFICA_STRUTTURA);
        }

        if (id == R.id.eliminastruttura){

            AlertDialog.Builder builder = new AlertDialog.Builder(DettaglioStruttura.this);
            builder.setTitle("Cancellazione");
            builder.setMessage("Sicuro di voler eliminare la struttura " + struttura.getNome() + "?");
            builder.setPositiveButton(R.string.sicuro, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int risultato = dbManager.eliminaTupla(DBColumns.S_TABLE, struttura.getId(), null);

                    if (risultato != -1) {

                        Toast.makeText(getApplicationContext(), "Eliminazione avvenuta con successo.", Toast.LENGTH_SHORT).show();

                                /*Usando finish() invece di invocare con un intent l'activity, evito il problema che se da ListaStrutture
                                premo il pulsante back, mi torni alla scherma DettaglioStruttura con la struttura appena cancellata*/
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Non è stato possibile eliminare la struttura", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            builder.setNegativeButton(R.string.no_sicuro, null);

            builder.show();
        }

        if(id == R.id.helperstruttura){

            AlertDialog.Builder builder = new AlertDialog.Builder(DettaglioStruttura.this);
            builder.setTitle(R.string.helper_title);
            builder.setMessage(R.string.helper_struttura_message);
            builder.setPositiveButton("Ho capito", null);
            builder.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == AppUtils.RICHIESTA_MODIFICA_STRUTTURA)
            popolaElementi(data, AppUtils.STRUTTURA);
        else if(requestCode == AppUtils.PASSAGGIO_A_VALUTAZIONI) {

            Cursor fetchvalutazioni;
            double rate = 0;
            int count;

            fetchvalutazioni = dbManager.recuperaTuple(DBColumns.V_TABLE, DBColumns.V_STR + "= ?", new String[]{Long.toString(struttura.getId())});

            if(fetchvalutazioni.moveToFirst()){
                for(count = 0; count < fetchvalutazioni.getCount(); count++) {
                    rate += fetchvalutazioni.getDouble(fetchvalutazioni.getColumnIndex(DBColumns.V_RATE));
                    fetchvalutazioni.moveToNext();
                }

                rate = rate/count;
                rate = AppUtils.controllaRate(rate);

                if (rate != struttura.getRate()) {

                    struttura.setRate(rate);
                    labRating.setText("Rating: " + rate + "/5.0");
                    labRating.setVisibility(View.VISIBLE);
                }
            } else {
                labRating.setText("");
                labRating.setVisibility(View.GONE);
            }
        }
    }

    //funzione per l'inizializzazione degli elementi della view
    private void inizializzaElementi(){

        //recupero i riferimenti agli elementi della view
        imageStruttura = (ImageView) findViewById(R.id.image_struttura);
        labNome = (TextView) findViewById(R.id.nome_struttura);
        labCategoria = (TextView) findViewById(R.id.categoria_struttura);
        labRating = (TextView) findViewById(R.id.rate_struttura);
        labIndirizzo = (TextView) findViewById(R.id.labelInd);
        labTel = (TextView) findViewById(R.id.labelTel);
        labCell = (TextView) findViewById(R.id.labelCell);
        labMail = (TextView) findViewById(R.id.labelMail);
        labWeb = (TextView) findViewById(R.id.labelWeb);
        indirizzo = (TextView) findViewById(R.id.ind_struttura);
        tel = (TextView) findViewById(R.id.tel_struttura);
        cell = (TextView) findViewById(R.id.cell_struttura);
        mail = (TextView) findViewById(R.id.mail_struttura);
        web = (TextView) findViewById(R.id.web_struttura);
        descrizione = (TextView) findViewById(R.id.descrizione_struttura);
        esperienze = (Button) findViewById(R.id.esperienze_struttura);
    }

    //funzione per il popolamento degli elementi per l'intent ricevuto da ListaStruttura
    private void popolaElementi(Intent intent, String extra){

        struttura = (Struttura) intent.getSerializableExtra(extra);

        if(!struttura.getFotoPath().equals(""))
            impostaImmagine(struttura.getFotoPath());
        else
            AppUtils.impostaImmagineDefault(imageStruttura, struttura.getCategoria());

        labNome.setText(struttura.getNome());

        labCategoria.setText(struttura.getCategoria());

        if(struttura.getRate() != 0) {

            labRating.setText("Rating: " + struttura.getRate() + "/5");
            labRating.setVisibility(View.VISIBLE);
        }
        else
            labRating.setVisibility(View.GONE);

        if(!struttura.getTelefono().equals("")) {

            tel.setText(struttura.getTelefono());
            tel.setVisibility(View.VISIBLE);
            labTel.setVisibility(View.VISIBLE);
            tel.setOnClickListener(telOnClick);
            TableRow row = (TableRow) findViewById(R.id.row_tel);
            row.setVisibility(View.VISIBLE);
        }
        else{

            TableRow row = (TableRow) findViewById(R.id.row_tel);
            row.setVisibility(View.GONE);
        }

        if(!struttura.getIndirizzo().equals("")) {

            indirizzo.setText(struttura.getIndirizzo());
            indirizzo.setVisibility(View.VISIBLE);
            labIndirizzo.setVisibility(View.VISIBLE);
            indirizzo.setOnClickListener(indirizzoOnClick);
            TableRow row = (TableRow) findViewById(R.id.row_ind);
            row.setVisibility(View.VISIBLE);
        }
        else{

            TableRow row = (TableRow) findViewById(R.id.row_ind);
            row.setVisibility(View.GONE);
        }

        if(!struttura.getCellulare().equals("")) {

            cell.setText(struttura.getCellulare());
            cell.setVisibility(View.VISIBLE);
            labCell.setVisibility(View.VISIBLE);
            cell.setOnClickListener(cellOnClick);
            TableRow row = (TableRow) findViewById(R.id.row_cel);
            row.setVisibility(View.VISIBLE);
        }
        else{

            TableRow row = (TableRow) findViewById(R.id.row_cel);
            row.setVisibility(View.GONE);
        }

        if(!struttura.getMail().equals("")) {

            mail.setText(struttura.getMail());
            mail.setVisibility(View.VISIBLE);
            labMail.setVisibility(View.VISIBLE);
            mail.setOnClickListener(mailOnClick);
            TableRow row = (TableRow) findViewById(R.id.row_mail);
            row.setVisibility(View.VISIBLE);
        }
        else{

            TableRow row = (TableRow) findViewById(R.id.row_mail);
            row.setVisibility(View.GONE);
        }

        if(!struttura.getSitoweb().equals("")) {

            web.setText(struttura.getSitoweb());
            web.setVisibility(View.VISIBLE);
            labWeb.setVisibility(View.VISIBLE);
            web.setOnClickListener(webOnClick);
            TableRow row = (TableRow) findViewById(R.id.row_sito);
            row.setVisibility(View.VISIBLE);
        }
        else{

            TableRow row = (TableRow) findViewById(R.id.row_sito);
            row.setVisibility(View.GONE);
        }

        if(!struttura.getDescrizione().equals("")){

            descrizione.setText(struttura.getDescrizione());
            descrizione.setVisibility(View.VISIBLE);
        }
        else
            descrizione.setVisibility(View.GONE);

        esperienze.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DettaglioStruttura.this, ListaValutazioni.class);
                intent.putExtra(AppUtils.STRUTTURA, struttura.getId());
                startActivityForResult(intent, AppUtils.PASSAGGIO_A_VALUTAZIONI);
            }
        });
    }

    //imposta l'immagine della struttura, recuperandola dal file system
    private void impostaImmagine(String path){
        imageStruttura.setImageBitmap(AppUtils.getImageOriented(path, AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT));
    }
}
