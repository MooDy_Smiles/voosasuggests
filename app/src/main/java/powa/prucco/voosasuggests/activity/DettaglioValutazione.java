package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBColumns;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Valutazione;
import powa.prucco.voosasuggests.utility.AppUtils;

public class DettaglioValutazione extends AppCompatActivity {

    private static final String SEARCH_PREFIX = "www.google.it/?gws_rd=ssl#q=";

    private DBManager dbManager;

    private TextView datada, labDataA, dataa, rate, descrizione;
    private Valutazione valutazione;

    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_dettaglio_valutazione);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        dbManager = new DBManager(this);

        inizializzaElementi();

        //recupero l'intent passato da ListaValutazioni
        Intent intent = getIntent();

        if(intent != null && intent.hasExtra(AppUtils.VALUTAZIONE))
            popolaElementi(intent, AppUtils.VALUTAZIONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dettaglio_valutazione, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.modificavalutazione) {
            Intent perModifica = new Intent(DettaglioValutazione.this, AggiungiOModificaValutazione.class);
            perModifica.putExtra(AggiungiOModificaValutazione.MODIFICA, valutazione);
            startActivityForResult(perModifica, AppUtils.RICHIESTA_MODIFICA_VALUTAZIONE);
        }

        if(id == R.id.eliminavalutazione) {

            AlertDialog.Builder builder = new AlertDialog.Builder(DettaglioValutazione.this);
            builder.setTitle("Cancellazione");
            builder.setMessage("Sei sicuro di voler eliminare la valutazione?");
            builder.setPositiveButton(R.string.sicuro, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int risultato = dbManager.eliminaTupla(DBColumns.V_TABLE, valutazione.getIdStruttura(), valutazione.getDatada());

                    if (risultato == -1) {
                        Toast.makeText(getApplicationContext(), "Eliminazione avvenuta con successo.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Non è stato possibile eliminare la valutazione", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            builder.setNegativeButton(R.string.no_sicuro, null);

            builder.show();

        }

        if(id == R.id.condividivalutazione) {
            if(AppUtils.connessioneInternet(DettaglioValutazione.this)){
                SharedPreferences sharedPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE);
                if(sharedPreferences.getBoolean("logged", false)) {

                    condividiPost();
                } else {
                    new AlertDialog.Builder(DettaglioValutazione.this)
                            .setTitle(R.string.no_facebook)
                            .setMessage("Non sei connesso con il tuo account di facebook, desideri accedere?")
                            .setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(DettaglioValutazione.this, FacebookAccount.class));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            } else
                Toast.makeText(DettaglioValutazione.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == AppUtils.RICHIESTA_MODIFICA_VALUTAZIONE)
            popolaElementi(data, AppUtils.VALUTAZIONE);
    }

    private void inizializzaElementi(){

        datada = (TextView) findViewById(R.id.datada_valutazione);
        dataa = (TextView) findViewById(R.id.dataa_valutazione);
        labDataA = (TextView) findViewById(R.id.labDataA);
        rate = (TextView) findViewById(R.id.rating_valutazione);
        descrizione = (TextView) findViewById(R.id.descrizione_valutazione);
    }

    private void popolaElementi(Intent intent, String extra){

        valutazione = (Valutazione) intent.getSerializableExtra(extra);

        datada.setText(valutazione.getDatada());

        if(!valutazione.getDataa().equals("")) {
            dataa.setText(valutazione.getDataa());
            dataa.setVisibility(View.VISIBLE);
            labDataA.setVisibility(View.VISIBLE);
        }
        else{
            labDataA.setVisibility(View.GONE);
            dataa.setVisibility(View.GONE);
        }

        rate.setText(valutazione.getRate() + "/5.0");

        descrizione.setText(valutazione.getDescrizione());
    }

    private void condividiPost() {

        Cursor recupero = dbManager.recuperaTuple(DBColumns.S_TABLE, DBColumns.S_ID + "= ?", new String[]{
                Long.toString(valutazione.getIdStruttura())
        });

        if(recupero != null && recupero.moveToFirst()) {

            if(recupero.getCount() == 1) {

                String nomeStruttura = recupero.getString(recupero.getColumnIndex(DBColumns.S_NOME));
                String sitoweb = recupero.getString(recupero.getColumnIndex(DBColumns.S_WEB));

                String linkUri = "";
                String descrizione = "";

                if(!sitoweb.equals(""))
                    linkUri = sitoweb;
                else
                    linkUri = SEARCH_PREFIX + nomeStruttura;

                if (!valutazione.getDataa().equals(""))
                    descrizione += "Dal " + valutazione.getDatada() + " al " + valutazione.getDataa();
                else
                    descrizione += "Il " + valutazione.getDatada();

                descrizione += ", valutata " + valutazione.getRate() + "/5.0!";

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentDescription(descrizione)
                        .setContentTitle("La mia esperienza a " + nomeStruttura)
                        .setContentUrl(Uri.parse(linkUri))
                        .build();

                shareDialog.show(linkContent);
            }else{
                Log.e("VoosaSuggests", "Recuperata più di 1 struttura: " + recupero.getCount() + " tuple recuperate");
            }
        }
    }

    private String impostaUriDaDrawable(long categoria) {

        Cursor recupero = dbManager.recuperaTuple(DBColumns.C_TABLE, DBColumns.C_ID + "= ?", new String[]{
                Long.toString(categoria)
        });

        String toReturn = "";

        if(recupero != null && recupero.moveToFirst()){

            if(recupero.getCount() == 1) {

                final String nomeCategoria = recupero.getString(recupero.getColumnIndex(DBColumns.C_DESC));
                int resID;
                switch (nomeCategoria){
                    case "Agriturismo" : resID = R.drawable.icon_agriturismo; break;
                    case "Casa Scout" : resID = R.drawable.icon_casascout; break;
                    case "Hotel" : resID = R.drawable.icon_hotel; break;
                    case "Ristorante" : resID = R.drawable.icon_ristorante; break;
                    default : resID = R.drawable.icon_default; break;
                }

                toReturn = ContentResolver.SCHEME_ANDROID_RESOURCE
                        + "://" + getResources().getResourcePackageName(resID)
                        + '/' + getResources().getResourceTypeName(resID)
                        + '/' + getResources().getResourceEntryName(resID);
                Log.d("VoosaSuggests", "Path to drawable: " + toReturn);
            }else{
                toReturn = ContentResolver.SCHEME_ANDROID_RESOURCE
                        + "://" + getResources().getResourcePackageName(R.drawable.icon_default)
                        + '/' + getResources().getResourceTypeName(R.drawable.icon_default)
                        + '/' + getResources().getResourceEntryName(R.drawable.icon_default);
                Log.e("VoosaSuggests", "Recuperata più di 1 categoria: " + recupero.getCount() + " tuple recuperate");
                Log.d("VoosaSuggests", "Path to drawable: " + toReturn);
            }
        }

        return toReturn;
    }
}
