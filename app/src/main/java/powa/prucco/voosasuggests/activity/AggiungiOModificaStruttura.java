package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBColumns;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Struttura;
import powa.prucco.voosasuggests.utility.AppUtils;
import powa.prucco.voosasuggests.utility.Pair;
import powa.prucco.voosasuggests.utility.SpinnerAdapter;

public class AggiungiOModificaStruttura extends AppCompatActivity {

    public static final String MODIFICA = "MODIFICA";
    public static final String AGGIUNGI = "AGGIUNGI";

    private DBManager dbManager;
    private Struttura struttura = null;
    private ArrayAdapter<Pair> adapter;
    private Intent intent;
    private String picturePath = "";
    private File photo;

    private ImageView imageStruttura;
    private EditText nome, indirizzo, telefono, cellulare, mail, web, descrizione, focus;
    private Spinner categoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aggiungi_omodifica_struttura);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DBManager(this);

        inizializzaElementi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_aggiungi_omodifica_struttura, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.salvastruttura){

            //eseguo controllo per duplicazione di valore nome struttura
            Cursor duplicato = dbManager.recuperaTuple(DBColumns.S_TABLE, DBColumns.S_NOME + "= ?", new String[]{nome.getText().toString()});

            if(duplicato.moveToFirst()){
                Long idCheck = duplicato.getLong(duplicato.getColumnIndex(DBColumns.S_ID));
                if(struttura == null || struttura.getId() != idCheck) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaStruttura.this);
                    builder.setTitle("Duplicato");
                    builder.setMessage("Elemento con lo stesso nome già presente in memoria.");
                    builder.setPositiveButton("Ho capito.", null);

                    builder.show();

                    nome.setText("");
                    nome.requestFocus();

                    return super.onOptionsItemSelected(item);
                }
            }

            if(nome.getText().toString().equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaStruttura.this);
                builder.setTitle("Errore nel form");
                builder.setPositiveButton("Ok", null);
                builder.setMessage("Form incompleto.\nIl nome struttura è obbligatorio.");

                builder.show();

                nome.requestFocus();
            } else {

                if(struttura == null)
                    struttura = new Struttura();

                Pair coppia = (Pair)categoria.getSelectedItem();

                struttura.setNome(nome.getText().toString());
                struttura.setIndirizzo(indirizzo.getText().toString());
                struttura.setTelefono(telefono.getText().toString());
                struttura.setCellulare(cellulare.getText().toString());
                struttura.setMail(mail.getText().toString());
                struttura.setSitoweb(web.getText().toString());
                struttura.setDescrizione(descrizione.getText().toString());
                struttura.setId_categoria(coppia.getId());
                struttura.setCategoria(coppia.getNome());
                struttura.setFotoPath(picturePath);

                Intent risultato = new Intent();

                if(intent != null && intent.hasExtra(MODIFICA)) {
                    if(dbManager.aggiornaStruttura(struttura) == null){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaStruttura.this);
                        builder.setTitle("Errore");
                        builder.setPositiveButton("Ok", null);
                        builder.setMessage("Salvataggio delle modifiche nel database non riuscito.");

                        builder.show();

                        return super.onOptionsItemSelected(item);
                    }
                } else {
                    if(dbManager.aggiungiStruttura(struttura) == null){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaStruttura.this);
                        builder.setTitle("Errore");
                        builder.setPositiveButton("Ok", null);
                        builder.setMessage("Salvataggio delle modifiche nel database non riuscito.");

                        builder.show();

                        return super.onOptionsItemSelected(item);
                    }
                }

                risultato.putExtra(AppUtils.STRUTTURA, struttura);

                setResult(RESULT_OK, risultato);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppUtils.RICHIESTA_IMMAGINE_GALLERIA && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            imageStruttura.setImageBitmap(AppUtils.getScaledBitmap(picturePath, AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT));
        } else if(requestCode == AppUtils.RICHIESTA_IMMAGINE_CAMERA && resultCode == RESULT_OK){

            picturePath = photo.getAbsolutePath();
            AggiungiOModificaStruttura.this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(picturePath)));
            //imageStruttura.setImageBitmap(AppUtils.getScaledBitmap(picturePath, 110, 110));
            imageStruttura.setImageBitmap(AppUtils.getImageOriented(picturePath, AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT));

            int i = 2;
        } else if(requestCode == AppUtils.RICHIESTA_IMMAGINE_CAMERA && resultCode == RESULT_CANCELED){

            photo.delete();
        }
    }

    //funzione per l'inizializzazione degli elementi della view
    private void inizializzaElementi(){

        imageStruttura = (ImageView) findViewById(R.id.image_struttura);
        nome = (EditText) findViewById(R.id.nome_struttura);
        indirizzo = (EditText) findViewById(R.id.ind_struttura);
        telefono = (EditText) findViewById(R.id.tel_struttura);
        cellulare = (EditText) findViewById(R.id.cell_struttura);
        mail = (EditText) findViewById(R.id.mail_struttura);
        web = (EditText) findViewById(R.id.web_struttura);
        descrizione = (EditText) findViewById(R.id.descrizione_struttura);
        categoria = (Spinner) findViewById(R.id.categoria_struttura);
        popolaSpinner();
        impostaListenerImmagine();

        intent = getIntent();

        //recupero eventuali dati passati da DettaglioStruttura
        if(intent != null && intent.hasExtra(MODIFICA)) {
            struttura = (Struttura) intent.getSerializableExtra(MODIFICA);

            popolaElementi();
        }

        focus = (EditText) findViewById(R.id.toLoseFocus);
        focus.requestFocus();
    }

    private void popolaElementi(){

        if(!struttura.getFotoPath().equals("")){
            impostaImmagine(struttura.getFotoPath());
            picturePath = struttura.getFotoPath();
        } else {
            AppUtils.impostaImmagineDefault(imageStruttura, struttura.getCategoria());
        }
        nome.setText(struttura.getNome());
        indirizzo.setText(struttura.getIndirizzo());
        telefono.setText(struttura.getTelefono());
        cellulare.setText(struttura.getCellulare());
        mail.setText(struttura.getMail());
        web.setText(struttura.getSitoweb());
        descrizione.setText(struttura.getDescrizione());

        /*Pair fake = new Pair(struttura.getCategoria(), struttura.getId_categoria());

        int position = adapter.getPosition(fake);
        categoria.setSelection(position);*/
        int position = 0;
        for(int i = 0; i < adapter.getCount(); i++){
            if(adapter.getItem(i).getNome().equals(struttura.getCategoria()))
                position = i;
        }

        categoria.setSelection(position);
    }

    //imposta l'immagine della struttura, recuperandola dal file system
    private void impostaImmagine(String path){
        File imageFile = new File(path);
        if(imageFile.exists()){
            //Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            imageStruttura.setImageBitmap(AppUtils.getImageOriented(path, AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT));
        }
    }

    //funzione per l'inizializzazione dello spinner
    private void popolaSpinner(){

        Cursor recuperaCategorie = dbManager.recuperaTuple(DBColumns.C_TABLE,null,null);
        Pair toAdd;

        if (recuperaCategorie == null) {
            Toast.makeText(this, "Impossibile caricare categorie dal database.", Toast.LENGTH_SHORT).show();
            return;
        } else if(!recuperaCategorie.moveToFirst()){
            Toast.makeText(this, "Nessuna categoria presente nel database.", Toast.LENGTH_SHORT).show();
            return;
        }

        List<Pair> catList = new ArrayList<Pair>();
        adapter = new SpinnerAdapter(this, R.layout.spinner_layout_item, catList);


        for (int i = 0; i < recuperaCategorie.getCount(); i++) {
            toAdd = new Pair(recuperaCategorie.getString(recuperaCategorie.getColumnIndex(DBColumns.C_DESC)),  recuperaCategorie.getLong(recuperaCategorie.getColumnIndex(DBColumns.C_ID)));
            catList.add(toAdd);
            recuperaCategorie.moveToNext();
        }
        adapter.setDropDownViewResource(R.layout.spinner_layout_dropdown_item);
        categoria.setAdapter(adapter);
        categoria.setSelection(0);
    }

    //funzione per la gestione del onClickListener sull'immagine
    private void impostaListenerImmagine(){
        imageStruttura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String[] voci;

                if(struttura == null || struttura.getFotoPath().equals(""))
                    voci = new String[]{"Camera","File System", "Annulla"};
                else
                    voci = new String[]{"Camera","File System","Imposta default","Annulla"};

                AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaStruttura.this);
                builder.setTitle("Scegli opzione");
                builder.setItems(voci, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (voci[which]) {
                            case "Camera":
                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                                    try {
                                        photo = AppUtils.createImageFile();
                                    } catch (Exception e) {
                                        Toast.makeText(AggiungiOModificaStruttura.this, "Impossibile scattare foto", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                    if (photo != null) {
                                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                        startActivityForResult(Intent.createChooser(takePictureIntent, "Scegli applicazione"), AppUtils.RICHIESTA_IMMAGINE_CAMERA);
                                    }
                                }
                                break;

                            case "File System":
                                Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                startActivityForResult(Intent.createChooser(intent, "Scegli immagine"), AppUtils.RICHIESTA_IMMAGINE_GALLERIA);
                                break;

                            case "Imposta default":
                                picturePath = "";
                                AppUtils.impostaImmagineDefault(imageStruttura, struttura.getCategoria());
                                break;

                            case "Annulla":
                                dialog.dismiss();
                                break;
                        }
                    }
                });

                builder.show();
            }
        });
    }
}