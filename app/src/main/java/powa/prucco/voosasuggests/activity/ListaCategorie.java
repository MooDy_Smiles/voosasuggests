package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBColumns;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Categoria;
import powa.prucco.voosasuggests.tabelle.Struttura;
import powa.prucco.voosasuggests.utility.AppUtils;
import powa.prucco.voosasuggests.utility.CategoriaAdapter;

public class ListaCategorie extends AppCompatActivity {

    private static CategoriaAdapter categoriaAdapter = null;
    private static List<Categoria> categoriaList = new ArrayList<>();
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_categorie);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DBManager(this);

        categoriaAdapter = new CategoriaAdapter(this, categoriaList);

        ListView list = (ListView) findViewById(R.id.listCategorie);
        list.setAdapter(categoriaAdapter);

        if(categoriaList.isEmpty())
            caricaCategorie();

        registerForContextMenu(list);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_categorie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.aggiungicategoria) {

            //provo utilizzando una dialog

            AlertDialog.Builder builder = new AlertDialog.Builder(ListaCategorie.this);
            LayoutInflater layoutInflater = ListaCategorie.this.getLayoutInflater();

            View myView = layoutInflater.from(ListaCategorie.this).inflate(R.layout.custom_dialog_aggiungiomodifica_categoria, null);

            //builder.setView(layoutInflater.inflate(R.layout.custom_dialog_aggiungiomodifica_categoria, null));
            builder.setView(myView);
            builder.setTitle("Aggiungi Categoria");

            final EditText nome = (EditText) myView.findViewById(R.id.nome_categoria_bis);

            builder.setPositiveButton(R.string.aggiungi, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Cursor duplicato = dbManager.recuperaTuple(DBColumns.C_TABLE, DBColumns.C_DESC + "= ?", new String[]{nome.getText().toString()});

                    if(duplicato.moveToFirst()){
                        new AlertDialog.Builder(ListaCategorie.this)
                                .setTitle("Duplicato")
                                .setMessage("Elemento con lo stesso nome già presente in memoria.")
                                .setPositiveButton("Ho capito.", null)
                                .show();

                        nome.setText("");
                        nome.requestFocus();

                    }

                    if(nome.getText().toString().equals("")){
                        new AlertDialog.Builder(ListaCategorie.this)
                                .setTitle("Errore nel form")
                                .setPositiveButton("Ok", null)
                                .setMessage("Form incompleto.\nIl nome categoria è obbligatorio.")
                                .create()
                                .show();
                    } else {

                        Categoria categoria = new Categoria();

                        categoria.setNome(nome.getText().toString());

                        if(dbManager.aggiungiCategoria(categoria) == null) {
                            new AlertDialog.Builder(ListaCategorie.this)
                                    .setPositiveButton("Ok", null)
                                    .setMessage("Salvataggio delle modifiche nel database non riuscito.")
                                    .create()
                                    .show();
                        } else {

                            categoriaList.add(categoria);
                            categoriaAdapter.notifyDataSetChanged();

                            dialog.dismiss();
                        }
                    }
                }
            });

            builder.setNegativeButton("Annulla", null);

            builder.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        categoriaList.clear();
        caricaCategorie();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        if(v.getId() == R.id.listCategorie){
            menu.setHeaderTitle(categoriaList.get(info.position).getNome());
            String[] azioni = getResources().getStringArray(R.array.options);
            for(int i = 0; i < azioni.length; i++)
                menu.add(Menu.NONE, i, i, azioni[i]);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] azioni = getResources().getStringArray(R.array.options);
        String azione = azioni[menuItemIndex];

        final Categoria categoria = categoriaList.get(info.position);

        switch(azione){
            case "Modifica" : {

                //provo utilizzando una dialog

                AlertDialog.Builder builder = new AlertDialog.Builder(ListaCategorie.this);
                LayoutInflater layoutInflater = ListaCategorie.this.getLayoutInflater();

                View myView = layoutInflater.from(ListaCategorie.this).inflate(R.layout.custom_dialog_aggiungiomodifica_categoria, null);

                builder.setView(myView);

                builder.setTitle("Modifica Categoria");

                final EditText nome = (EditText) myView.findViewById(R.id.nome_categoria_bis);
                nome.setText(categoria.getNome());

                builder.setPositiveButton(R.string.modifica,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(nome.getText().equals("")){
                            new AlertDialog.Builder(ListaCategorie.this)
                                    .setPositiveButton("Ok", null)
                                    .setTitle("Errore nel form")
                                    .setMessage("Form incompleto.\nIl nome categoria è obbligatorio.")
                                    .create()
                                    .show();
                        } else {

                            categoria.setNome(nome.getText().toString());

                            if(dbManager.aggiornaCategoria(categoria) == null) {
                                new AlertDialog.Builder(ListaCategorie.this)
                                        .setTitle("Errore")
                                        .setPositiveButton("Ok", null)
                                        .setMessage("Salvataggio delle modifiche nel database non riuscito.")
                                        .create()
                                        .show();
                            } else {

                                categoriaAdapter.notifyDataSetChanged();

                                dialog.dismiss();
                            }
                        }
                    }
                });

                builder.setNegativeButton("Annulla", null);

                builder.show();

                break;
            }
            case "Elimina" : {

                if(!categoria.getNome().equals("Default")) {

                    new AlertDialog.Builder(ListaCategorie.this)
                            .setTitle("Cancellazione")
                            .setMessage("Sicuro di voler eliminare la categoria " + categoria.getNome() + "?")
                            .setPositiveButton(R.string.sicuro, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    int risultato = dbManager.eliminaTupla(DBColumns.C_TABLE, categoria.getId(), null);

                                    if (risultato != -1) {
                                        //la cancellazione è avvenuta con successo

                                        categoriaList.remove(categoria);
                                        categoriaAdapter.notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(ListaCategorie.this, "Non è stato possibile eliminare la categoria " + categoria.getNome(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            })
                            .setNegativeButton(R.string.no_sicuro, null)
                            .create()
                            .show();

                } else {
                    Toast.makeText(ListaCategorie.this, "Non puoi eliminare la categoria di default.", Toast.LENGTH_LONG).show();
                }
            }
        }

        return true;
    }

    private void caricaCategorie(){

        Cursor recupero = dbManager.recuperaTuple(DBColumns.C_TABLE, null, null);

        if (recupero == null) {
            Toast.makeText(this, "Impossibile caricare dati dal database.", Toast.LENGTH_SHORT).show();
            return;
        } else if(!recupero.moveToFirst()){
            Toast.makeText(this, "Nessuna struttura presente nel database.", Toast.LENGTH_SHORT).show();
            return;
        }

        for(int i = 0; i < recupero.getCount(); i++){
            long id = recupero.getLong(recupero.getColumnIndex(DBColumns.C_ID));
            String nome = recupero.getString(recupero.getColumnIndex(DBColumns.C_DESC));

            Categoria categoria = new Categoria();
            categoria.setId(id);
            categoria.setNome(nome);

            categoriaList.add(categoria);

            recupero.moveToNext();
        }

        categoriaAdapter.notifyDataSetChanged();
    }
}
