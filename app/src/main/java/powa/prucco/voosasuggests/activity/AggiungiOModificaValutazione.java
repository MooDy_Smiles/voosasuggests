package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Struttura;
import powa.prucco.voosasuggests.tabelle.Valutazione;
import powa.prucco.voosasuggests.utility.AppUtils;

public class AggiungiOModificaValutazione extends AppCompatActivity {

    public static final String MODIFICA = "MODIFICA";
    public static final String AGGIUNGI = "AGGIUNGI";

    private final Calendar calendario = Calendar.getInstance();

    private TextView addDataA, rate;
    private EditText dataDa, dataA, descrizione, focus;
    private RatingBar rater;
    private ImageView cancelData;

    private long id_struttura;
    private Intent intent;
    private Valutazione valutazione = null;
    private DBManager dbManager;

    private View.OnClickListener cancelDataOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dataA.setText("");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aggiungi_omodifica_valutazione);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DBManager(this);

        intent = getIntent();

        inizializzaElementi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_aggiungi_omodifica_valutazione, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.salva_valutazione){
            if(dataDa.getText().toString().equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaValutazione.this);
                builder.setTitle("Errore nel form");
                builder.setPositiveButton("Ok", null);
                builder.setMessage("Form incompleto.\nLa data di inizio è obbligatoria.");

                builder.show();

                dataDa.requestFocus();
            } else {
                if(valutazione == null)
                    valutazione = new Valutazione();

                valutazione.setDatada(dataDa.getText().toString());
                valutazione.setDataa(dataA.getText().toString());
                valutazione.setRate((double) rater.getRating());
                valutazione.setIdStruttura(id_struttura);
                valutazione.setDescrizione(descrizione.getText().toString());

                Intent risultato = new Intent();

                if(intent != null && intent.hasExtra(MODIFICA)){
                    if(dbManager.aggiornaValutazione(valutazione) == null){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaValutazione.this);
                        builder.setTitle("Errore");
                        builder.setPositiveButton("Ok", null);
                        builder.setMessage("Salvataggio delle modifiche nel database non riuscito.");

                        builder.show();

                        return super.onOptionsItemSelected(item);
                    }
                } else {
                    if(dbManager.aggiungiValutazione(valutazione) == null){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AggiungiOModificaValutazione.this);
                        builder.setTitle("Errore");
                        builder.setPositiveButton("Ok", null);
                        builder.setMessage("Salvataggio delle modifiche nel database non riuscito.");

                        builder.show();

                        return super.onOptionsItemSelected(item);
                    }
                }

                risultato.putExtra(AppUtils.VALUTAZIONE, valutazione);

                setResult(RESULT_OK, risultato);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void inizializzaElementi(){

        addDataA = (TextView) findViewById(R.id.addDataA);
        dataDa = (EditText) findViewById(R.id.dataDa);
        dataA = (EditText) findViewById(R.id.dataA);
        //imposto a disabilitato così sono sicuro di mettere una data di fine solo se
        //è presente una data di inizio
        dataA.setEnabled(false);
        cancelData = (ImageView) findViewById(R.id.dataADelete);
        cancelData.setOnClickListener(cancelDataOnClick);
        descrizione = (EditText) findViewById(R.id.descrizioneValutazione);
        rater = (RatingBar) findViewById(R.id.rater);
        rater.setNumStars(5);
        rate = (TextView) findViewById(R.id.rateIndicator);
        rate.setText("0.0/5.0");
        focus = (EditText) findViewById(R.id.toLostFocus);
        focus.requestFocus();

        if(intent.hasExtra(MODIFICA)) {
            valutazione = (Valutazione) intent.getSerializableExtra(MODIFICA);
            id_struttura = valutazione.getIdStruttura();
            popolaElementi();
        }
        else {
            id_struttura = intent.getLongExtra(AGGIUNGI, -1);
        }

        if(id_struttura < 0){
            Toast.makeText(AggiungiOModificaValutazione.this, "ID struttura non valido, qui qualquadra non cosa!", Toast.LENGTH_SHORT).show();
            finish();
        }

        impostaListeners();
    }

    //Imposta i listener per i datepicker e la ratingbar
    private void impostaListeners(){

        rater.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //DecimalFormat decimalFormat = new DecimalFormat("#.#");
                rate.setText(rating + "/5.0");
            }
        });

        final DatePickerDialog.OnDateSetListener dataDaListener = new DatePickerDialog.OnDateSetListener(){

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayofMonth) {
                impostaCalendario(dayofMonth, monthOfYear, year);
                impostaFormatoEditText(dataDa);
                dataA.setEnabled(true);
            }
        };

        dataDa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AggiungiOModificaValutazione.this, dataDaListener,
                        calendario.get(Calendar.YEAR), calendario.get(Calendar.MONTH), calendario.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener dataAListener = new DatePickerDialog.OnDateSetListener(){

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayofMonth){
                impostaCalendario(dayofMonth, monthOfYear, year);
                impostaFormatoEditText(dataA);
            }
        };

        dataA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(AggiungiOModificaValutazione.this, dataAListener,
                        calendario.get(Calendar.YEAR), calendario.get(Calendar.MONTH), calendario.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMinDate(calendario.getTimeInMillis() + AppUtils.ONE_DAY_DELAY);

                datePickerDialog.show();
            }
        });
    }


    private void impostaCalendario(int day,int month,int year){
        calendario.set(Calendar.DAY_OF_MONTH, day);
        calendario.set(Calendar.MONTH, month);
        calendario.set(Calendar.YEAR, year);

    }

    private void impostaFormatoEditText(EditText date){
        date.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY).format(calendario.getTime()));
    }

    private void popolaElementi(){
        dataDa.setText(valutazione.getDatada());
        dataA.setEnabled(true);
        if(!valutazione.getDataa().equals(""))
            dataA.setText(valutazione.getDataa());
        rater.setRating((float) valutazione.getRate());
        rate.setText(valutazione.getRate()+"/5.0");
        if(!valutazione.getDescrizione().equals(""))
            descrizione.setText(valutazione.getDescrizione());
    }
}
