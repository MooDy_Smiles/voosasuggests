package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import powa.prucco.voosasuggests.R;

public class FacebookAccount extends AppCompatActivity {

    private SharedPreferences myPreferences;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private LoginButton loginButton;
    private TextView information;
    private ProfilePictureView profilePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_facebook_account);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE);
        information = (TextView) findViewById(R.id.basicInformation);
        profilePic = (ProfilePictureView) findViewById(R.id.picProfile);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                //callback.onSuccess(loginResult);
                loginButton.clearPermissions();
                loginButton.setPublishPermissions("publish_actions");
                loginButton.setPublishPermissions();

                SharedPreferences.Editor editor = myPreferences.edit();
                editor.putBoolean("logged", true);
                editor.apply();
                displayMessage(Profile.getCurrentProfile());
            }

            @Override
            public void onCancel() {
                Toast.makeText(FacebookAccount.this, "Login annullato.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                new AlertDialog.Builder(FacebookAccount.this)
                        .setTitle("Facebook Error")
                        .setMessage("Errore nella funzionalità: " + e.getMessage())
                        .setPositiveButton("Ok.", null)
                        .show();
            }
        });

        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

                if(newToken != null) {

                    SharedPreferences.Editor editor = myPreferences.edit();
                    editor.putBoolean("logged", true);
                    editor.apply();
                } else if (newToken == null){

                    SharedPreferences.Editor editor = myPreferences.edit();
                    editor.putBoolean("logged", false);
                    editor.apply();
                    information.setText("");
                    profilePic.setProfileId(null);
                }

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                if (newProfile != null) {
                    SharedPreferences.Editor editor = myPreferences.edit();
                    editor.putBoolean("logged", true);
                    editor.apply();
                }

            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        return super.onCreateView(parent, name, context, attrs);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    @Override
    protected void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void displayMessage(Profile profile){
        if(profile != null) {
            information.setText(profile.getName());
            profilePic.setProfileId(profile.getId());
        }
    }

}
