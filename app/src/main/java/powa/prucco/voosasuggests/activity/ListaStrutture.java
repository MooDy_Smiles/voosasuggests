package powa.prucco.voosasuggests.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.database.DBColumns;
import powa.prucco.voosasuggests.database.DBManager;
import powa.prucco.voosasuggests.tabelle.Struttura;
import powa.prucco.voosasuggests.utility.StrutturaAdapter;
import powa.prucco.voosasuggests.utility.AppUtils;

public class ListaStrutture extends AppCompatActivity{

    private static final String TAG = "ListaStrutture";

    private ListView listStrutture;

    private static StrutturaAdapter strutturaAdapter = null;
    private static List<Struttura> strutturaList = new ArrayList<>();
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_lista_strutture);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DBManager(this);

        strutturaAdapter = new StrutturaAdapter(this, strutturaList);

        listStrutture = (ListView) findViewById(R.id.listStrutture);
        listStrutture.setAdapter(strutturaAdapter);

        if(strutturaList.isEmpty())
            caricaStrutture("");

        impostaListenerListStrutture();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_strutture, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.categorie) {
            Intent intent = new Intent(ListaStrutture.this, ListaCategorie.class);
            startActivity(intent);
        }

        if (id == R.id.aggiungi_struttura){
            //Toast.makeText(getApplicationContext(),"Bravoh! Hai premuto il pulsante per aggiungere una nuova struttura.", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(ListaStrutture.this, AggiungiOModificaStruttura.class), AppUtils.RICHIESTA_AGGIUNTA_STRUTTURA);
        }

        if (id == R.id.cambia_visualizzazione){
            visualizzaScelte();
        }

        if (id == R.id.facebook){
            startActivity(new Intent(ListaStrutture.this, FacebookAccount.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && (requestCode == AppUtils.RICHIESTA_AGGIUNTA_STRUTTURA || requestCode == AppUtils.RICHIESTA_MODIFICA_STRUTTURA)){
            strutturaList.clear();
            caricaStrutture("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        strutturaList.clear();
        caricaStrutture("");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        if(v.getId() == R.id.listStrutture){
            menu.setHeaderTitle(strutturaList.get(info.position).getNome());
            String[] azioni = getResources().getStringArray(R.array.options);
            for(int i = 0; i < azioni.length; i++)
                menu.add(Menu.NONE, i, i, azioni[i]);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] azioni = getResources().getStringArray(R.array.options);
        String azione = azioni[menuItemIndex];

        final Struttura struttura = strutturaList.get(info.position);

        switch(azione){
            case "Modifica" : {
                Intent perModifica = new Intent(ListaStrutture.this, AggiungiOModificaStruttura.class);
                perModifica.putExtra(AggiungiOModificaStruttura.MODIFICA, struttura);
                startActivityForResult(perModifica, AppUtils.RICHIESTA_MODIFICA_STRUTTURA);
                break;
            }
            case "Elimina" : {

                AlertDialog.Builder builder = new AlertDialog.Builder(ListaStrutture.this);
                builder.setTitle("Cancellazione");
                builder.setMessage("Sicuro di voler eliminare la struttura " + struttura.getNome() + "?");
                builder.setPositiveButton(R.string.sicuro, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int risultato = dbManager.eliminaTupla(DBColumns.S_TABLE, struttura.getId(), null);

                        if (risultato != -1) {
                            //cancellazione avvenuta con successo
                            strutturaList.remove(struttura);
                            strutturaAdapter.notifyDataSetChanged();
                        }
                    }
                });
                builder.setNegativeButton(R.string.no_sicuro, null);

                builder.show();
            }
        }

        return true;
    }

    //funzione per l'assegnazione dei listener alla listView
    private void impostaListenerListStrutture(){

        listStrutture.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Struttura struttura = (Struttura) adapterView.getItemAtPosition(i);

                Intent intent = new Intent(ListaStrutture.this, DettaglioStruttura.class);
                intent.putExtra(AppUtils.STRUTTURA, struttura);
                startActivity(intent);
            }
        });

        registerForContextMenu(listStrutture);
    }

    //funzione recupero lista di strutture da DB
    private void caricaStrutture(String filtro){

        Cursor recupero;
        Cursor fetchcategoria;
        Cursor fetchvalutazioni;
        String nomecategoria = "";
        String extra = "";
        double rate = 0;
        int count;

        if(filtro.equals(""))
            recupero = dbManager.recuperaTuple(DBColumns.S_TABLE, null, null);
        else
            recupero = dbManager.recuperaTuple(DBColumns.S_TABLE, DBColumns.S_CAT + "= ?", new String[]{filtro});

        if (recupero == null) {
            Toast.makeText(this, "Impossibile caricare dati dal database.", Toast.LENGTH_SHORT).show();
            return;
        } else if(!recupero.moveToFirst()){
            if(!filtro.equals("")) extra = "di categoria ";
            Toast.makeText(this, "Nessuna struttura " + extra + "presente nel database.", Toast.LENGTH_SHORT).show();
            return;
        }

        for (int i = 0; i < recupero.getCount(); i++) {
            long id = recupero.getLong(recupero.getColumnIndex(DBColumns.S_ID));
            String nome = recupero.getString(recupero.getColumnIndex(DBColumns.S_NOME));
            String indirizzo = recupero.getString(recupero.getColumnIndex(DBColumns.S_IND));
            String telefono = recupero.getString(recupero.getColumnIndex(DBColumns.S_TEL));
            String cellulare = recupero.getString(recupero.getColumnIndex(DBColumns.S_CELL));
            String sitoweb = recupero.getString(recupero.getColumnIndex(DBColumns.S_WEB));
            String mail = recupero.getString(recupero.getColumnIndex(DBColumns.S_MAIL));
            String descrizione = recupero.getString(recupero.getColumnIndex(DBColumns.S_DESC));
            String foto = recupero.getString(recupero.getColumnIndex(DBColumns.S_FOTO));
            long idCategoria = recupero.getLong(recupero.getColumnIndex(DBColumns.S_CAT));

            fetchvalutazioni = dbManager.recuperaTuple(DBColumns.V_TABLE, DBColumns.V_STR + "= ?", new String[]{Long.toString(id)});

            rate = 0;
            if(fetchvalutazioni.moveToFirst()){
                for(count = 0; count < fetchvalutazioni.getCount(); count++) {
                    rate += fetchvalutazioni.getDouble(fetchvalutazioni.getColumnIndex(DBColumns.V_RATE));
                    fetchvalutazioni.moveToNext();
                }

                rate = rate/count;
                rate = AppUtils.controllaRate(rate);
            }

            if(!foto.equals(""))
                foto = AppUtils.controllaFoto(foto);

            fetchcategoria = dbManager.recuperaTuple(DBColumns.C_TABLE, DBColumns.C_ID + "= ?", new String[]{Long.toString(idCategoria)});

            if(fetchcategoria == null){
                Toast.makeText(this, "Impossibile recuperare la categoria.", Toast.LENGTH_SHORT).show();
                return;
            }else if(!fetchcategoria.moveToFirst()){
                Toast.makeText(this, "Nessuna corrispondeza per il codice categoria trovata.", Toast.LENGTH_SHORT).show();
                return;
            }

            nomecategoria = fetchcategoria.getString(fetchcategoria.getColumnIndex(DBColumns.C_DESC));

            Struttura struttura = new Struttura();
            struttura.setId(id);
            struttura.setNome(nome);
            struttura.setRate(rate);
            struttura.setIndirizzo(indirizzo);
            struttura.setTelefono(telefono);
            struttura.setCellulare(cellulare);
            struttura.setSitoweb(sitoweb);
            struttura.setMail(mail);
            struttura.setDescrizione(descrizione);
            struttura.setFotoPath(foto);
            struttura.setId_categoria(idCategoria);
            struttura.setCategoria(nomecategoria);

            strutturaList.add(struttura);

            recupero.moveToNext();
        }

        strutturaAdapter.notifyDataSetChanged();
    }

    //funzione per la popolazione della Choidce Dialog
    private void visualizzaScelte(){
        Cursor recupero = dbManager.recuperaTuple(DBColumns.C_TABLE, null, null);

        if (recupero == null) {
            Toast.makeText(this, "Impossibile caricare dati dal database.", Toast.LENGTH_SHORT).show();
            return;
        } else if(!recupero.moveToFirst()){
            Toast.makeText(this, "Nessuna struttura presente nel database.", Toast.LENGTH_SHORT).show();
            return;
        }

        final TreeMap<String, String> voci = new TreeMap<>();
        voci.put("Tutte",new String("0"));

        for(int i = 1; i <= recupero.getCount(); i++){
            voci.put(recupero.getString(recupero.getColumnIndex(DBColumns.C_DESC)),
                    Long.toString(recupero.getLong(recupero.getColumnIndex(DBColumns.C_ID))));
            recupero.moveToNext();
        }

        //final String[] items = (String[]) voci.values().toArray();
        final String[] items = new String[voci.size()];
        voci.keySet().toArray(items);

        AlertDialog.Builder builder = new AlertDialog.Builder(ListaStrutture.this);
        builder.setTitle(R.string.visualizzazione);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                strutturaList.clear();
                strutturaAdapter.notifyDataSetChanged();
                if (items[item].equals("Tutte"))
                    caricaStrutture("");
                else
                    caricaStrutture(voci.get(items[item]));
                dialog.dismiss();

            }
        });

        builder.show();
    }
}