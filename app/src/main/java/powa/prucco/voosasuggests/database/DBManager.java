package powa.prucco.voosasuggests.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import powa.prucco.voosasuggests.tabelle.*;

/**
 * Created by Andrea on 07/08/2015.
 */
public class DBManager {

    private static final String TAG = "DBManager::Class";

    private DBHelper dbHelper;

    //Inizializzazione dell'oggetto {@link DBHelper}
    public DBManager(Context context){
        dbHelper = new DBHelper(context);
    }

    //Funzione per il recupero delle tuple dalla tabella nometabella
    public Cursor recuperaTuple(String nometabella, String condizione, String[] valori){

        Cursor crs = null;

        try{
            SQLiteDatabase database = dbHelper.getReadableDatabase();

            if((condizione == null || condizione.equals("")) && (valori == null || valori.length == 0 ))
                crs = database.query(nometabella, null, null, null, null, null, null);
            else
                crs = database.query(nometabella, null, condizione, valori, null, null, null);
            Log.w(TAG, "Recupero delle tuple avvenuto con successo per la tabella " + nometabella);
        } catch (Exception e){
            Log.e(TAG, "Errore nel recuperare le tuple: " + e.toString());
        }

        return crs;
    }

    //Funzione per l'eliminazione di una o più tuple dalla tabella CATEGORIE o STRUTTURE
    public int eliminaTupla(String nometabella, long id, String data){

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        int risultato = -1;

        if (nometabella.equals(DBColumns.S_TABLE)) {

            //Eliminazione dalla tabella STRUTTURE, con la conseguenza di dover
            //eliminare tutte le esperienze pregresse dalla tabella VALUTAZIONI
            risultato = controllaValutazioni(database, id);

            if(risultato != -1)
                risultato = database.delete(DBColumns.S_TABLE, DBColumns.S_ID + " = ?", new String[]{Long.toString(id)});

        } else if(nometabella.equals(DBColumns.C_TABLE)){

            //Eliminazione dalla tabella CATEGORIE

            risultato = database.delete(DBColumns.C_TABLE, DBColumns.C_ID + " = ?", new String[]{Long.toString(id)});

            //eliminazione avvenuta, aggiorno le strutture che avevano la categoria
            // appena cancellata impostando quella di default
            if(risultato != -1) {
                String update = "UPDATE " + DBColumns.S_TABLE + " SET " + DBColumns.S_CAT + "= 1"
                        + " WHERE " + DBColumns.S_CAT + "= " + Long.toString(id);
                database.execSQL(update);
            }

        } else {

            //Eliminazione dalla tabella VALUTAZIONI

            risultato = database.delete(DBColumns.V_TABLE, DBColumns.V_DATADA + " = ? AND "
                    + DBColumns.V_STR + " = ?", new String[]{data, Long.toString(id)});

            int i = 0;
        }

        return risultato;
    }

    //Funzione per l'inserimento di una tupla in CATEGORIE
    public Categoria aggiungiCategoria(Categoria categoria){

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DBColumns.C_DESC, categoria.getNome());

        long risultato = database.insert(DBColumns.C_TABLE, null, content);

        if(risultato == -1)
            return null;

        categoria.setId(risultato);

        return categoria;
    }

    //Funzione per l'inserimento di una tupla in CATEGORIE
    public Struttura aggiungiStruttura(Struttura struttura){

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DBColumns.S_NOME, struttura.getNome());
        content.put(DBColumns.S_IND, struttura.getIndirizzo());
        content.put(DBColumns.S_TEL, struttura.getTelefono());
        content.put(DBColumns.S_CELL, struttura.getCellulare());
        content.put(DBColumns.S_WEB, struttura.getSitoweb());
        content.put(DBColumns.S_MAIL, struttura.getMail());
        content.put(DBColumns.S_DESC, struttura.getDescrizione());
        content.put(DBColumns.S_FOTO, struttura.getFotoPath());
        content.put(DBColumns.S_CAT, struttura.getId_categoria());

        long risultato = database.insert(DBColumns.S_TABLE, null, content);

        if(risultato == -1)
            return null;

        struttura.setId(risultato);

        return struttura;
    }

    //Funzione per l'inserimento di una tupla in VALUTAZIONI
    public Valutazione aggiungiValutazione(Valutazione valutazione){
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DBColumns.V_DATADA, valutazione.getDatada());
        content.put(DBColumns.V_DATAA, valutazione.getDataa());
        content.put(DBColumns.V_STR, valutazione.getIdStruttura());
        content.put(DBColumns.V_RATE, valutazione.getRate());
        content.put(DBColumns.V_DESC, valutazione.getDescrizione());

        long risultato = database.insert(DBColumns.V_TABLE, null, content);

        if(risultato == -1)
            return null;

        valutazione.setId(risultato);

        return valutazione;
    }

    //Funzione per l'aggiornamento di una tupla in CATEGORIE
    public Categoria aggiornaCategoria(Categoria categoria){

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DBColumns.C_DESC, categoria.getNome());

        long risultato = -1;

        try {
            risultato = database.update(DBColumns.C_TABLE, content, DBColumns.C_ID + "= ?", new String[]{
                    Long.toString(categoria.getId())
            });
        } catch (Exception e){
            Log.e(TAG, "Errore nel salvataggio delle modifiche " + e.toString());
        }

        if(risultato == 0)
            return null;

        return categoria;
    }

    //Funzione per l'aggiornamento di una tupla in STRUTTURE
    public Struttura aggiornaStruttura(Struttura struttura){

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DBColumns.S_NOME, struttura.getNome());
        content.put(DBColumns.S_IND, struttura.getIndirizzo());
        content.put(DBColumns.S_TEL, struttura.getTelefono());
        content.put(DBColumns.S_CELL, struttura.getCellulare());
        content.put(DBColumns.S_WEB, struttura.getSitoweb());
        content.put(DBColumns.S_MAIL, struttura.getMail());
        content.put(DBColumns.S_DESC, struttura.getDescrizione());
        content.put(DBColumns.S_FOTO, struttura.getFotoPath());
        content.put(DBColumns.S_CAT, struttura.getId_categoria());

        long risultato = -1;

        try {
            risultato = database.update(DBColumns.S_TABLE, content, DBColumns.S_ID + "= ?", new String[]{
                    Long.toString(struttura.getId())
            });
        } catch (Exception e){
            Log.e(TAG, "Errore nel salvataggio delle modifiche " + e.toString());
        }

        if(risultato == 0)
            return null;

        return struttura;
    }

    //Funzione per l'aggiornamento di una tupla in VALUTAZIONI
    public Valutazione aggiornaValutazione(Valutazione valutazione){
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(DBColumns.V_DATADA, valutazione.getDatada());
        content.put(DBColumns.V_DATAA, valutazione.getDataa());
        content.put(DBColumns.V_STR, valutazione.getIdStruttura());
        content.put(DBColumns.V_RATE, valutazione.getRate());
        content.put(DBColumns.V_DESC, valutazione.getDescrizione());

        long risultato = -1;

        try {
            risultato = database.update(DBColumns.V_TABLE, content, DBColumns.V_ID + "= ?", new String[]{
                    Long.toString(valutazione.getId())
            });
        } catch (Exception e){
            Log.e(TAG, "Errore nel salvataggio delle modifiche " + e.toString());
        }

        if(risultato == 0)
            return null;

        return valutazione;
    }

    //funzione per il controllo della presenza di valutazioni per la struttura che si sta
    //andando ad eliminare, nel qual caso si eliminano anche quelle
    private int controllaValutazioni(SQLiteDatabase database, long id){

        int operazione = -1;

        try {
            Cursor count = database.rawQuery("select count(*) from " + DBColumns.V_TABLE
                    + " where " + DBColumns.V_STR + "='" + Long.toString(id) + "'", null);

            count.moveToFirst();
            operazione = count.getInt(0);
            count.close();

            if (operazione == 0) {
                Log.i(TAG, "Nessuna ricorrenza da cancellare dalla tabella " + DBColumns.V_TABLE + " per la struttura con ID: " + id);
                return operazione;
            }

            operazione = database.delete(DBColumns.V_TABLE, DBColumns.V_STR + " = ?", new String[]{Long.toString(id)});
            Log.i(TAG, "Cancellate " + operazione + " ricorrenze dalla tabella " + DBColumns.V_TABLE);
        } catch (Exception e){
            Log.e(TAG, "Errore durante eliminazione valutazioni per struttura con ID: " + id +".\n" + e.toString());
        }


        return operazione;
    }
}
