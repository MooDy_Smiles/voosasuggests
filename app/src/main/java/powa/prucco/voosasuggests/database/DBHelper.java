package powa.prucco.voosasuggests.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Andrea on 07/08/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "voosafacilities";
    public static final int DB_VERSION = 1;

    // TABELLA CATEGORIE
    private static final String CREA_TABELLA_CATEGORIE = "CREATE TABLE "
            + DBColumns.C_TABLE + " (" + DBColumns.C_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DBColumns.C_DESC +" TEXT NOT NULL)";

    // TABELLA STRUTTURE
    private static final String CREA_TABELLA_STRUTTURE = "CREATE TABLE "
            + DBColumns.S_TABLE + " (" + DBColumns.S_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DBColumns.S_NOME + " TEXT NOT NULL,"
            + DBColumns.S_IND + " TEXT," + DBColumns.S_TEL + " TEXT,"
            + DBColumns.S_CELL + " TEXT," + DBColumns.S_WEB + " TEXT,"
            + DBColumns.S_MAIL + " TEXT," + DBColumns.S_DESC + " TEXT,"
            + DBColumns.S_FOTO + " TEXT," + DBColumns.S_CAT + " INTEGER,"
            + "FOREIGN KEY(" + DBColumns.S_CAT + ") REFERENCES " + DBColumns.C_TABLE + "("+DBColumns.C_ID+"))";
    //+ DBColumns.S_NOME + " TEXT NOT NULL," + DBColumns.S_RATE + " TEXT,"
    //TABELLA VALUTAZIONI

    private static final String CREA_TABELLA_VALUTAZIONI = "CREATE TABLE "
            + DBColumns.V_TABLE + " (" + DBColumns.V_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DBColumns.V_DATADA + " TEXT NOT NULL," + DBColumns.V_DATAA
            + " TEXT," + DBColumns.V_STR + " INTEGER NOT NULL," + DBColumns.V_RATE
            + " REAL NOT NULL," + DBColumns.V_DESC + " TEXT,"
            + " FOREIGN KEY(" + DBColumns.V_STR + ") REFERENCES " + DBColumns.S_TABLE + "("+DBColumns.S_ID+"))";

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Creazione delle tabelle
        db.execSQL(CREA_TABELLA_CATEGORIE);
        db.execSQL(CREA_TABELLA_STRUTTURE);
        db.execSQL(CREA_TABELLA_VALUTAZIONI);

        //Inserimento dati di default
        doInit(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }

    // Esegue l'inserimento dei dati di default
    private void doInit(SQLiteDatabase db){
        ContentValues values1 = new ContentValues();
        values1.put(DBColumns.C_DESC, "Default");

        long result1 = db.insert(DBColumns.C_TABLE, null, values1);

        ContentValues values2 = new ContentValues();
        values2.put(DBColumns.C_DESC, "Hotel");

        long result2 = db.insert(DBColumns.C_TABLE, null, values2);

        ContentValues values3 = new ContentValues();
        values3.put(DBColumns.C_DESC, "Ristorante");

        long result3 = db.insert(DBColumns.C_TABLE, null, values3);

        ContentValues values4 = new ContentValues();
        values4.put(DBColumns.C_DESC, "Agriturismo");

        long result4 = db.insert(DBColumns.C_TABLE, null, values4);

        ContentValues values5 = new ContentValues();
        values5.put(DBColumns.C_DESC, "Casa scout");

        long result5 = db.insert(DBColumns.C_TABLE, null, values5);

        //Valori di struttura prova solo per test
        ContentValues values6 = new ContentValues();
        values6.put(DBColumns.S_NOME, "Struttura Prova");
        values6.put(DBColumns.S_IND, "Via cella 18, Coriano");
        values6.put(DBColumns.S_TEL, "0541656152");
        values6.put(DBColumns.S_CELL, "3348125645");
        values6.put(DBColumns.S_WEB, "www.mangareader.net");
        values6.put(DBColumns.S_MAIL, "diphlo.powa@gmail.com");
        values6.put(DBColumns.S_DESC, "Questo è il testo di prova per la descrizione della struttura.\n" +
                "Allungo un po' il brodo così vedo se funziona la scrollView.");
        values6.put(DBColumns.S_FOTO, "");
        values6.put(DBColumns.S_CAT, "1");

        long result6 = db.insert(DBColumns.S_TABLE, null, values6);
    }
}
