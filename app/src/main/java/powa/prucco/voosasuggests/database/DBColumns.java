package powa.prucco.voosasuggests.database;

/**
 * Created by Andrea on 07/08/2015.
 */
public class DBColumns {

    //FORMATO DATA
    public static final String DATEFORMAT = "";

    //DATI TABELLA CATEGORIE
    public static final String C_TABLE = "categorie";
    public static final String C_ID = "categoriaID";
    public static final String C_DESC =    "descrizionecategoria";

    //DATI TABELLA STRUTTURE
    public static final String S_TABLE = "strutture";
    public static final String S_ID = "strutturaID";
    public static final String S_NOME = "strutturanome";
    public static final String S_RATE = "strutturavalutazione";
    public static final String S_IND = "strutturaindirizzo";
    public static final String S_TEL = "strutturatelefono";
    public static final String S_CELL = "strutturacellulare";
    public static final String S_WEB = "strutturasitoweb";
    public static final String S_MAIL = "strutturaemail";
    public static final String S_DESC = "descrizionestruttura";
    public static final String S_FOTO = "fotostruttura";
    public static final String S_CAT = "strutturacategoria";

    //DATI TABELLA VALUTAZIONI
    public static final String V_TABLE = "valutazioni";
    public static final String V_ID = "valutazioneID";
    public static final String V_DATADA = "valutazionedatada";
    public static final String V_DATAA = "valutazionedataa";
    public static final String V_STR = "valutazionestruttura";
    public static final String V_RATE = "valutazionevalutazione";
    public static final String V_DESC = "valutazionedescrizione";
}
