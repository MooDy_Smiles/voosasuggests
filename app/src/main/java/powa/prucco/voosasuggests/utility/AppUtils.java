package powa.prucco.voosasuggests.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import powa.prucco.voosasuggests.R;

/**
 * Created by Andrea on 20/08/2015.
 */
public class AppUtils {

    public static final int RICHIESTA_AGGIUNTA_STRUTTURA = 15;
    public static final int RICHIESTA_MODIFICA_STRUTTURA = 25;
    public static final int RICHIESTA_AGGIUNTA_VALUTAZIONE = 35;
    public static final int RICHIESTA_MODIFICA_VALUTAZIONE = 45;

    public static final int RICHIESTA_IMMAGINE_CAMERA = 16;
    public static final int RICHIESTA_IMMAGINE_GALLERIA = 26;

    public static final int PASSAGGIO_A_VALUTAZIONI = 17;

    public static final String STRUTTURA = "Struttura";
    public static final String VALUTAZIONE = "Valutazione";

    public static final int IMAGE_WIDTH = 110;
    public static final int IMAGE_HEIGHT = 110;

    public static final String APP_DIRECTORY = "/VoosaSuggests/";

    public static final long ONE_DAY_DELAY = 86400000;

    public static Bitmap getScaledBitmap(String picturePath, int width, int height) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, sizeOptions);

        int inSampleSize = calculateInSampleSize(sizeOptions, width, height);

        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;

        return BitmapFactory.decodeFile(picturePath, sizeOptions);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    //funzione per l'impostazione dell'immagine di default nel caso non ve ne sia stata data una
    public static void impostaImmagineDefault(ImageView view, String categoria){
        int resID;

        switch(categoria){
            case "Agriturismo": resID = R.drawable.icon_agriturismo; break;
            case "Casa scout" : resID = R.drawable.icon_casascout; break;
            case "Default" : resID = R.drawable.icon_default; break;
            case "Hotel" : resID = R.drawable.icon_hotel; break;
            case "Ristorante" : resID = R.drawable.icon_ristorante; break;
            default : resID = R.drawable.icon_default; break;
        }

        view.setImageResource(resID);
    }

    //funzione per il salvataggio della foto ottenuta dalla camera
    public static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "voosasuggests_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + APP_DIRECTORY);
        if(!storageDir.exists())
            storageDir.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return image;
    }

    //funzione per il controllo della presenza della foto nel file system
    public static String controllaFoto(String path){

        File toCheck = new File(path);
        if(!toCheck.exists())
            path = "";

        return path;
    }

    //funzione per l'impostazione del rate nel range di 0.5
    public static double controllaRate(double rate){

        double thing;

        thing = rate - (int)rate;

        if(0.25 <= thing && thing <= 0.75){
            thing = (int)rate + 0.5;
        } else if(thing < 0.25){
            thing = (int)rate;
        } else{
            thing = (int)rate + 1;
        }

        return thing;
    }

    //funzione per il controllo della presenza di connessione ad Internet
    public static boolean connessioneInternet(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //funzione per il controllo della rotazione dell'immagine
    private static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static Bitmap getImageOriented(String picturePath, int width, int height){

        Bitmap photo =  getScaledBitmap(picturePath, width, height);

        Matrix matrix = new Matrix();
        matrix.postRotate(getImageOrientation(picturePath));
        photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(),
                photo.getHeight(), matrix, true);

        return photo;
    }
}
