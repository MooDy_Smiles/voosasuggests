package powa.prucco.voosasuggests.utility;

/**
 * Created by andrea.pruccoli on 16/09/2015.
 */
public class Pair {

    private String nome;
    private long id;

    public Pair(String nome, long id){
        this.nome = nome;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.getNome();
    }
}
