package powa.prucco.voosasuggests.utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.tabelle.Valutazione;

/**
 * Created by Andrea on 04/09/2015.
 */
public class ValutazioneAdapter extends ArrayAdapter<Valutazione> {

    public ValutazioneAdapter(Context context, List<Valutazione> valutazioni){
        super(context, 0, valutazioni);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.list_item_valutazione, parent, false);

        TextView datada = (TextView) convertView.findViewById(R.id.datada);
        TextView rating = (TextView) convertView.findViewById(R.id.rating);
        TextView descrizione = (TextView) convertView.findViewById(R.id.descrizione);

        Valutazione valutazione = getItem(position);

        String data = valutazione.getDatada();
        if(!valutazione.getDataa().equals("") && valutazione.getDataa() != null)
            data = data + " - " + valutazione.getDataa();
        datada.setText(data);

        rating.setText(valutazione.getRate() + "/5.0");

        if(valutazione.getDescrizione().equals("")) {
            descrizione.setVisibility(View.GONE);

        }
        else
            descrizione.setText(valutazione.getDescrizione());

        return convertView;
    }
}
