package powa.prucco.voosasuggests.utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.tabelle.Categoria;

/**
 * Created by Andrea on 01/09/2015.
 */
public class CategoriaAdapter extends ArrayAdapter<Categoria> {

    private TextView nome;

    public CategoriaAdapter(Context context, List<Categoria> categorie){
        super(context, 0, categorie);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.list_item_categoria, parent, false);

        nome = (TextView) convertView.findViewById(R.id.categoria);

        Categoria categoria = getItem(position);

        nome.setText(categoria.getNome());

        return convertView;
    }
}
