package powa.prucco.voosasuggests.utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import powa.prucco.voosasuggests.R;

/**
 * Created by andrea.pruccoli on 14/10/2015.
 *
 * Riciclo il layout della categoria che va bene per il mio scopo
 */
public class ImpostazioniAdapter extends ArrayAdapter<String> {

    private TextView campo;

    public ImpostazioniAdapter(Context context, String[] campi){
        super(context, 0, campi);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.list_item_categoria, parent, false);

        campo = (TextView) convertView.findViewById(R.id.categoria);

        campo.setText((String)getItem(position));

        return convertView;
    }
}
