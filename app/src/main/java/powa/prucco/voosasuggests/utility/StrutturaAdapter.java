package powa.prucco.voosasuggests.utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import powa.prucco.voosasuggests.R;
import powa.prucco.voosasuggests.tabelle.Struttura;

/**
 * Created by Andrea on 17/08/2015.
 */
public class StrutturaAdapter extends ArrayAdapter<Struttura> {

    private ImageView foto;
    private TextView nome, categoria, rating;

    public StrutturaAdapter (Context context, List<Struttura> strutture){
        super(context, 0, strutture);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.list_item_struttura, parent, false);

        foto = (ImageView) convertView.findViewById(R.id.image_struttura);
        nome = (TextView) convertView.findViewById(R.id.nome_struttura);
        categoria = (TextView) convertView.findViewById(R.id.categoria_struttura);
        rating = (TextView) convertView.findViewById(R.id.rate_struttura);

        Struttura struttura = getItem(position);

        if(!struttura.getFotoPath().equals(""))
            foto.setImageBitmap(AppUtils.getImageOriented(struttura.getFotoPath(), AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT));
        else {
            AppUtils.impostaImmagineDefault(foto, struttura.getCategoria());
        }

        nome.setText(struttura.getNome());
        categoria.setText(struttura.getCategoria());
        if(struttura.getRate() != 0)
            rating.setText("Rating: " + struttura.getRate() + "/5");
        else
            rating.setVisibility(View.GONE);

        return convertView;
    }
}
