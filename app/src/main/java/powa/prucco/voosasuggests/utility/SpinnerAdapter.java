package powa.prucco.voosasuggests.utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import powa.prucco.voosasuggests.R;

/**
 * Created by Andrea on 21/08/2015.
 */
public class SpinnerAdapter extends ArrayAdapter<Pair> {

    public SpinnerAdapter(Context context, int resource, List<Pair> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {
        View mySpinner = LayoutInflater.from(getContext()).inflate(
                R.layout.spinner_layout_item, parent, false);

        TextView categoria = (TextView) mySpinner.findViewById(R.id.spinnerText);

        Pair coppia = getItem(position);

        categoria.setText(coppia.getNome());

        return mySpinner;
    }
}
